defmodule Katso.Repo.Migrations.AddTitleTotalScore do
  use Ecto.Migration

  def change do
    alter table(:titles) do
      add :total_score, :integer
    end
  end
end
