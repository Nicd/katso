defmodule Katso.Repo.Migrations.Init do
  use Ecto.Migration

  def change do
    create table(:magazines) do
      add :name, :string
      add :key, :string
    end

    create index(:magazines, [:key], unique: true)

    create table(:fetches) do
      add :magazine_id, references(:magazines)
      add :total_score, :integer
      add :total_titles, :integer
      add :relative_score, :integer

      timestamps
    end

    create table(:fetch_scores) do
      add :fetch_id, references(:fetches)
      add :score_type, :string
      add :score_amount, :integer
    end

    create table(:titles) do
      add :fetch_id, references(:fetches)
      add :title, :text
    end

    create table(:title_scores) do
      add :title_id, references(:titles)
      add :score_type, :string
      add :score_amount, :integer
      add :score_words, {:array, :text}
    end
  end
end
