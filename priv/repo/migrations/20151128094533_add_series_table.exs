defmodule Katso.Repo.Migrations.AddSeriesTable do
  use Ecto.Migration

  def change do
    create table(:series) do
      timestamps
    end

    alter table(:fetches) do
      add :series_id, references(:series)
    end
  end
end
