use Mix.Config

# In this file, we keep production configuration that
# you likely want to automate and keep it away from
# your version control system.
config :katso, Katso.Endpoint,
  secret_key_base: "vPhy4euw5ZRCoD6Oh9apXEuvyGSxY8v6mXQOummG970OnVYxFfPmQ50uHAYIqErk"

# Configure your database
config :katso, Katso.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: "postgres",
  password: "postgres",
  database: "katso_prod",
  pool_size: 20
