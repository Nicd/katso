defmodule Katso.PageController do
  use Katso.Web, :controller
  use Timex
  alias Katso.Repo
  alias Katso.Magazine
  alias Katso.Series
  alias Katso.Title
  import Ecto.Query, only: [from: 2, order_by: 3]

  def index(conn, _params) do
    series_data =
      from(s in Series,
        join: f in assoc(s, :fetches),
        join: m in assoc(f, :magazine),
        preload: [fetches: {f, magazine: m}],
        order_by: [asc: s.inserted_at])

      |> Repo.all()
      |> Enum.map(fn series ->
        data = Enum.sort(series.fetches, fn first, second -> first.magazine.id < second.magazine.id end)
          |> Enum.map(fn fetch -> fetch.relative_score end)

        [series.inserted_at | data]
      end)
      |> Poison.Encoder.encode([])

    magazines =
      from(m in Magazine,
        select: m.name,
        order_by: [asc: m.id])
      |> Repo.all
      |> Poison.Encoder.encode([])

    week_ago = Date.now |> Date.subtract(Time.to_timestamp(1, :weeks)) |> DateConvert.to_erlang_datetime

    top_titles =
      from(t in Title,
        join: f in assoc(t, :fetch),
        join: ts in assoc(t, :title_scores),
        join: m in assoc(f, :magazine),
        preload: [fetch: {f, magazine: m}, title_scores: ts],
        where: f.inserted_at > ^week_ago,
        order_by: [desc: t.total_score])
      |> Repo.all
      |> Enum.slice(0..10)

    conn
    |> assign(:all_series, series_data)
    |> assign(:magazines, magazines)
    |> assign(:top_titles, top_titles)
    |> render("index.html")
  end
end
