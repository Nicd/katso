defmodule Katso.Series do
  use Katso.Web, :model

  schema "series" do
    timestamps
    has_many :fetches, Katso.Fetch
  end

  @required_fields ~w()
  @optional_fields ~w()

  def changeset(model, params \\ nil) do
    model
    |> cast(params, @required_fields, @optional_fields)
  end
end

defimpl Poison.Encoder, for: Katso.Series do
  def encode(model, opts) do
    model
    |> Map.take([:inserted_at, :updated_at, :fetches])
    |> Poison.Encoder.encode(opts)
  end
end
