defmodule Katso.Fetch do
  use Katso.Web, :model

  schema "fetches" do
    field :total_score, :integer
    field :total_titles, :integer
    field :relative_score, :integer

    timestamps

    belongs_to :magazine, Katso.Magazine
    belongs_to :series, Katso.Series
    has_many :titles, Katso.Title
    has_many :fetch_scores, Katso.FetchScore
  end

  @required_fields ~w(total_score total_titles relative_score series_id magazine_id)
  @optional_fields ~w()

  def changeset(model, params \\ nil) do
    model
    |> cast(params, @required_fields, @optional_fields)
  end
end

defimpl Poison.Encoder, for: Katso.Fetch do
  def encode(model, opts) do
    model
    |> Map.take([:total_score, :total_titles, :relative_score, :magazine, :inserted_at, :updated_at])
    |> Poison.Encoder.encode(opts)
  end
end
