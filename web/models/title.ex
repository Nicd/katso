defmodule Katso.Title do
  use Katso.Web, :model

  schema "titles" do
    field :title, :string
    field :total_score, :integer

    belongs_to :fetch, Katso.Fetch
    has_many :title_scores, Katso.TitleScore
  end

  @required_fields ~w(title fetch_id total_score)
  @optional_fields ~w()

  def changeset(model, params \\ nil) do
    model
    |> cast(params, @required_fields, @optional_fields)
  end
end
