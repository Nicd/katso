defmodule Katso.FetchScore do
  use Katso.Web, :model

  schema "fetch_scores" do
    field :score_type, :string
    field :score_amount, :integer

    belongs_to :fetch, Katso.Fetch
  end

  @required_fields ~w(score_type score_amount fetch_id)
  @optional_fields ~w()

  def changeset(model, params \\ nil) do
    model
    |> cast(params, @required_fields, @optional_fields)
  end
end
