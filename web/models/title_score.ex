defmodule Katso.TitleScore do
  use Katso.Web, :model

  schema "title_scores" do
    field :score_type, :string
    field :score_amount, :integer
    field :score_words, {:array, :string}

    belongs_to :title, Katso.Title
  end

  @required_fields ~w(score_type score_amount score_words title_id)
  @optional_fields ~w()

  def changeset(model, params \\ nil) do
    model
    |> cast(params, @required_fields, @optional_fields)
  end
end
