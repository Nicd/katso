defmodule Katso.Magazine do
  use Katso.Web, :model

  schema "magazines" do
    field :name, :string
    field :key, :string

    has_many :fetches, Katso.Fetch
  end

  @required_fields ~w(name key)
  @optional_fields ~w()

  def changeset(model, params \\ nil) do
    model
    |> cast(params, @required_fields, @optional_fields)
  end
end

defimpl Poison.Encoder, for: Katso.Magazine do
  def encode(model, opts) do
    model
    |> Map.take([:name, :key])
    |> Poison.Encoder.encode(opts)
  end
end
