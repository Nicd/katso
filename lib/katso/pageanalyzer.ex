defmodule Katso.PageAnalyzer do
  @moduledoc """
  This module contains functionality to analyze a single page. First it scrapes the page using Scraper and then
  calculates the scores using TitleAnalyzer.
  """

  alias Katso.Repo
  alias Katso.Magazine
  alias Katso.Fetch
  alias Katso.FetchScore
  alias Katso.Title
  alias Katso.TitleScore
  alias Katso.Series

  import Ecto.Query, only: [from: 2]

  @sites %{
    iltalehti: %{
      name: "Iltalehti",
      url: "http://www.iltalehti.fi/",
      rules: [
        ".df-blk",
        "p.even a"
      ]
    },

    iltasanomat: %{
      name: "Ilta-Sanomat",
      url: "http://www.iltasanomat.fi/",
      rules: [
        "h2",
        "h3",
        "a div p",
        "a div.content"
      ]
    },

    aamulehti: %{
      name: "Aamulehti",
      url: "http://www.aamulehti.fi/",
      rules: [
        "h2 a",
        "h3 a"
      ]
    },

    hs: %{
      name: "Helsingin Sanomat",
      url: "http://www.hs.fi/",
      rules: [
        "h2 a",
        "h3 a",
        "li div a"
      ]
    },

    ts: %{
      name: "Turun Sanomat",
      url: "http://www.ts.fi/",
      rules: [
        "h1"
      ]
    },

    ksml: %{
      name: "Keskisuomalainen",
      url: "http://www.ksml.fi/",
      rules: [
        "h1 a",
        "h2 a",
        "h3 a"
      ]
    },

    kymensanomat: %{
      name: "Kymen Sanomat",
      url: "http://www.kymensanomat.fi/",
      rules: [
        "h1 a",
        "h2 a",
        "div.news-title a"
      ]
    },

    etelasaimaa: %{
      name: "Etelä-Saimaa",
      url: "http://www.esaimaa.fi/",
      rules: [
        "h1 a",
        "h2 a",
        "div.news-title a"
      ]
    },

    kouvolansanomat: %{
      name: "Kouvolan Sanomat",
      url: "http://www.kouvolansanomat.fi/",
      rules: [
        "h1 a",
        "h2 a",
        "div.news-title a"
      ]
    },

    ess: %{
      name: "Etelä-Suomen Sanomat",
      url: "http://www.ess.fi/",
      rules: [
        "h1 a"
      ]
    },

    forssa: %{
      name: "Forssan Lehti",
      url: "http://www.forssanlehti.fi/",
      rules: [
        "h1 a",
        "h3 a"
      ]
    },

    hameensanomat: %{
      name: "Hämeen Sanomat",
      url: "http://www.hameensanomat.fi/",
      rules: [
        "h1 a"
      ]
    },

    lapinkansa: %{
      name: "Lapin Kansa",
      url: "http://www.lapinkansa.fi/",
      rules: [
        "h2 a",
        "li a"
      ]
    },

    yle: %{
      name: "Yle Uutiset",
      url: "http://yle.fi/uutiset/",
      rules: [
        "h1 a"
      ]
    },

    karjalainen: %{
      name: "Karjalainen",
      url: "http://www.karjalainen.fi/",
      rules: [
        "h1 a",
        "h2 a",
        "h3 a",
        "h4 a"
      ]
    },

    kangasalan_sanomat: %{
      name: "Kangasalan Sanomat",
      url: "http://kangasalansanomat.fi/",
      rules: [
        "h1",
        "h2"
      ]
    },

    kaleva: %{
      name: "Kaleva",
      url: "http://www.kaleva.fi/",
      rules: [
        "dd a",
        "h2"
      ]
    },

    mtv: %{
      name: "MTV Uutiset",
      url: "http://www.mtv.fi/uutiset",
      rules: [
        "h2",
        "div.related a",
        "li a p",
        "li a span",
        "p.headline"
      ]
    },

    uusisuomi: %{
      name: "Uusi Suomi",
      url: "http://www.uusisuomi.fi/",
      rules: [
        "h2 a",
        "h4 a"
      ]
    },

    ilkka: %{
      name: "Ilkka",
      url: "http://www.ilkka.fi/",
      rules: [
        "h1 a"
      ]
    },

    maaseuduntulevaisuus: %{
      name: "Maaseudun Tulevaisuus",
      url: "http://www.maaseuduntulevaisuus.fi/",
      rules: [
        "h2 a",
        "span.title"
      ]
    },

    savonsanomat: %{
      name: "Savon Sanomat",
      url: "http://www.savonsanomat.fi/",
      rules: [
        "h1 a",
        "h2 a",
        "div.media-body a"
      ]
    },

    # taloussanomat: %{
    #   name: "Taloussanomat",
    #   url: "http://www.taloussanomat.fi/",
    #   rules: [
    #     "h1 a",
    #     "h2 a",
    #     "h3 a"
    #   ]
    # },

    verkkouutiset: %{
      name: "Verkkouutiset",
      url: "http://www.verkkouutiset.fi/",
      rules: [
        "span.headline"
      ]
    },
  }

  def analyze_all() do
    Map.keys(@sites)
    |> Enum.map(fn site_key -> Task.async Katso.PageAnalyzer, :analyze, [site_key] end)
    |> handle_responses
    |> calculate_scores
    |> reject_emptys
    |> print_scores
    |> store_data
  end

  def analyze(site_key) do
    site = @sites[site_key]

    data = Katso.Scraper.scrape(site)
      |> Enum.map(fn title -> {title, Katso.TitleAnalyzer.analyze title} end)

    IO.puts "Analyzed " <> site.name

    {site_key, data}
  end

  def handle_responses(task_list, data \\ [])

  def handle_responses([], data), do: data

  def handle_responses(task_list, data) do
    receive do
      msg -> case Task.find task_list, msg do
          nil -> handle_responses task_list, data
          {result, task} ->
            data = [handle_response(result) | data]
            handle_responses List.delete(task_list, task), data
        end
    end
  end

  def handle_response({site_key, result}) do
    initial_scores = %{
      relative_score: 0,
      total_score: 0,
      score_types: [],
      total_titles: 0,
      matches: []
    }

    scores = result
      |> Enum.reduce(initial_scores, fn {title, title_scores}, total_scores ->
          title_scores
          |> Enum.reduce(total_scores, fn {score_key, _, score_amount}, acc ->
              acc
              |> Map.put(:score_types, Keyword.put(acc.score_types, score_key, Keyword.get(acc.score_types, score_key, 0) + score_amount))
              |> Map.put :total_score, acc.total_score + score_amount
            end)
          |> Map.put(:matches, [{title, title_scores} | total_scores.matches])
          |> Map.put :total_titles, total_scores.total_titles + 1
        end)

    {site_key, scores}
  end

  def calculate_scores(data) do
    Enum.map data, fn {site_key, scores} ->
        case scores.total_titles do
          0 -> nil
          _ -> {site_key, %{scores | relative_score: ((scores.total_score / scores.total_titles * 100) |> Float.round |> trunc) }}
        end
      end
  end

  def reject_emptys(data) do
    Enum.reject data, fn x -> x == nil end
  end

  def print_scores(data) do
    Enum.map data, fn {site_key, scores} ->
        IO.puts Atom.to_string(site_key) <> ": " <> Integer.to_string scores.relative_score
        {site_key, scores}
      end
  end

  def store_data(data) do
    series = create_series

    Enum.each data, fn {site_key, scores} ->
        query = from m in Magazine,
          where: m.key == ^(Atom.to_string site_key)

        magazine = case Repo.one query do
          nil -> create_magazine @sites[site_key], site_key
          m -> m
        end

        fetch = create_fetch series, magazine, scores

        Enum.each scores.score_types, fn score_type ->
            create_fetch_score fetch, score_type
          end

        Enum.reject(scores.matches, fn {_, score_types} -> score_types == [] end)
        |> Enum.each fn {match, score_types} ->
            sum = Enum.reduce score_types, 0, fn {_, _, score_amount}, acc ->
                acc + score_amount
              end

            title = create_title fetch, match, sum

            Enum.each score_types, fn score_type ->
                create_title_score title, score_type
              end
          end
      end
  end

  defp create_series() do
    %Series{}
    |> Series.changeset(%{})
    |> Repo.insert
    |> ok_or_die
  end

  defp create_magazine(site, site_key) do
    %Magazine{}
    |> Magazine.changeset(%{
      name: site.name,
      key: Atom.to_string(site_key)
    })
    |> Repo.insert
    |> ok_or_die
  end

  defp create_fetch(series, magazine, scores) do
    %Fetch{}
    |> Fetch.changeset(%{
      series_id: series.id,
      total_score: scores.total_score,
      total_titles: scores.total_titles,
      relative_score: scores.relative_score,
      magazine_id: magazine.id
    })
    |> Repo.insert
    |> ok_or_die
  end

  defp create_title(fetch, title, total_score) do
    %Title{}
    |> Title.changeset(%{
      title: title,
      fetch_id: fetch.id,
      total_score: total_score
    })
    |> Repo.insert
    |> ok_or_die
  end

  defp create_title_score(title, {score_type, score_words, score_amount}) do
    %TitleScore{}
    |> TitleScore.changeset(%{
      score_type: Atom.to_string(score_type),
      score_words: score_words,
      score_amount: score_amount,
      title_id: title.id
    })
    |> Repo.insert
    |> ok_or_die
  end

  defp create_fetch_score(fetch, {score_type, score_amount}) do
    %FetchScore{}
    |> FetchScore.changeset(%{
      score_type: Atom.to_string(score_type),
      score_amount: score_amount,
      fetch_id: fetch.id
    })
    |> Repo.insert
    |> ok_or_die
  end

  defp ok_or_die({:ok, result}), do: result

  defp ok_or_die({:error, err}) do
    raise err
  end
end
