defmodule Katso.Scraper do
  @moduledoc """
  This module stores the list of sites to scrape and their scraping rules.
  """

  @doc """
  List of user agents to use, one will be picked randomly from this list.
  """
  @uas [
    "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36",
    "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.75.14 (KHTML, like Gecko) Version/7.0.3 Safari/7046A194A",
    "Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; AS; rv:11.0) like Gecko",
    "Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; Trident/4.0; InfoPath.2; SV1; .NET CLR 2.0.50727; WOW64)",
    "Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; Zune 4.0; InfoPath.3; MS-RTC LM 8; .NET4.0C; .NET4.0E)",
    "Mozilla/5.0 (Windows NT 6.3; rv:36.0) Gecko/20100101 Firefox/36.0",
    "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:24.0) Gecko/20100101 Firefox/24.0",
    "Opera/9.80 (X11; Linux i686; Ubuntu/14.10) Presto/2.12.388 Version/12.16"
  ]

  # Options for hackney
  @hackney follow_redirect: true

  @doc """
  Scrape readable lines from the given site using the site's scraping rules.
  """
  def scrape(site) do
    case HTTPoison.get site.url, [{:"User-Agent", Enum.at(@uas, :random.uniform(Enum.count(@uas)) - 1)}], [hackney: @hackney] do
      {:error, _} -> :error
      {:ok, %HTTPoison.Response{body: html}} -> scrape site, html
    end
  end

  def scrape(site, html) do
    parse_tree = Floki.parse html

    site.rules
    |> Enum.map_reduce([], fn rule, acc -> {nil, do_scrape(parse_tree, rule, acc)} end)
    |> Tuple.to_list
    |> Enum.at(1)
    |> Enum.map(fn elem -> Katso.Utils.convert_utf8 Floki.text elem end)
    |> Enum.reject(fn elem -> String.strip(elem) == "" end)
  end

  def do_scrape(parse_tree, rule, results) do
    Floki.find(parse_tree, rule)
    |> Enum.concat results
  end
end
