defmodule Katso.Utils do
  def convert_utf8(str) do
    case String.valid? str do
      true -> str
      false -> 
        String.codepoints(str)
        |> Enum.reduce "", fn codepoint, acc ->
            acc <> case codepoint do
              <<byte>> -> <<byte :: utf8>>
              char -> char
            end
          end
    end
  end
end
