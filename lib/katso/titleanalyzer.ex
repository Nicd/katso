defmodule Katso.TitleAnalyzer do
  @moduledoc """
  This module contains the tools for analyzing a single title or piece of text.
  """

  @doc """
  The regex rules which are used to look up shitty stuff from text.
  Also includes explanations and point ratings for them.
  """
  @rules %{
    vau: %{
      s: "Vau! Oho! Ja kaikenlainen muu ihmettely.",
      r: ~r/\b(?:vau|oho|ohh?oh?|hups(?:is)?|huh)\b/iu,
      p: 5
    },

    nyt: %{
      s: "Nyt puhuu X! Nyt se on X!",
      r: ~r/\b(?:nyt se on|nyt puhu[uv])\b/iu,
      p: 3
    },

    pronominit: %{
      s: "Epämääräiset pronominit joilla vältellään kertomasta, mistä puhutaan. Hän, tämä, nämä jne.",
      r: ~r/
      \b(?:
        tämä|tässä|tällä|tästä|tänne|tähän|tälle|tämän|tältä|tätä|tällaista|
        tämänlaiset|tämänlaisia|tämänlaista|
        nämä|näissä|näillä|näistä|näihin|näille|näiden|näiltä|näitä|näin|
        hän|hänessä|hänellä|hänestä|häneen|hänelle|hänen|häneltä|häntä|
        he|heissä|heillä|heistä|heihin|heille|heidän|heiltä|heitä|heistä|
        ne|niissä|niillä|niistä|niihin|niille|niiden|niiltä|niitä|niistä
        )(?:kin)?\b
      /iux,
      p: 3
    },

    kysymys: %{
      s: "Kysymykset otsikoissa. Yleensä näihin vastaus on ”ei”.",
      r: ~r/\?/,
      p: 2
    },

    huuto: %{
      s: "Huonoja otsikoita tehostetaan usein huutomerkillä!",
      r: ~r/!/u,
      p: 1
    },

    turhat_sanat: %{
      s: "Sanoja, jotka yleensä merkitsevät turhaa lööppiä ja joilla yritetään saada siitä dramaattisemman kuuloinen.",
      r: ~r/
        \b(?:
          kohu
          |raivostu
          |kauhunhetk
          |seksi
          |dramaatti
          |julkkis
          |bb\-
          |outo
          |rohke(?:at?|is)
          |paljast
          |raju
          |skandaal
          |mokat?\b
          |ällistyttä
          |mahtava
          |uskomat(?:t|o)
          |vihdoin
          |avautu
          |tilit(?:y|t)
          |hyytäv
          |jäätäv
          |et usko
          |kansa\b
          |testaa\b
          |arvaa
          |erikoi(?:s|n)
          |nolo(?:\b|a|i)
          |sensaatio
          |omitui
          |(?:päätä)?huim
        )
      /iux,
      p: 3
    },

    katso: %{
      s: "Kehotus katsomaan jotain lisäsisältöä, joka lähes poikkeuksetta on hyödytöntä.",
      r: ~r/\bkatso\b/iu,
      p: 5
    },

    some: %{
      s: "Sosiaalinen media on turhaa hömpötystä.",
      r: ~r/\bsome|twii?tt|peukut(?:u|t)|facebook|insta(?:gram)?|pinterest/iu,
      p: 1
    },

    arvaatko: %{
      s: "Arvaatko? Uskaltaisitko? Viitsisitkö? jne.",
      r: ~r/tko\b|tkö\b/iu,
      p: 2
    }
  }

  def analyze(str) do
    Map.keys(@rules)
    |> Enum.map(fn key ->
        rule = @rules[key]

        case run_re str, rule do
          nil -> nil
          matches -> {key, matches, Enum.count(matches) * rule.p}
        end
      end)
    |> Enum.reject fn x -> x == nil end
  end

  def run_re(str, %{r: r}) do
    case Regex.scan r, str do
      [] -> nil
      matches -> Enum.map matches, fn match -> Enum.at match, 0 end
    end
  end
end